package _02_recursive_functions;

import java.math.BigInteger;
import java.util.Scanner;

class Factorial {
    static int factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }

    static BigInteger factorialBig(int n) {
        if (n == 0 || n == 1) {
            return BigInteger.ONE;
        }
        return BigInteger.valueOf(n).multiply(factorialBig(n - 1));
    }

    public static void main(String[] args0) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i <= n; i++) {
            System.out.printf("%d! = %d\n", i, factorial(i));
            System.out.printf("%d! = %d\n", i, factorialBig(i));
        }
        scanner.close();
    }
}

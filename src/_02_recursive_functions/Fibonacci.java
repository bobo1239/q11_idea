package _02_recursive_functions;

import java.util.Scanner;

class Fibonacci {
    static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }
        return fibonacci(n-1) + fibonacci(n - 2);
    }

    public static void main(String[] args0) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.printf("F_%d: %d", n, fibonacci(n));
        scanner.close();
    }
}
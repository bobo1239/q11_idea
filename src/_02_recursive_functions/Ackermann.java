package _02_recursive_functions;

class Ackermann {
    static int ack(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return ack(m - 1, 1);
        } else {
            return ack(m - 1, ack(m, n - 1));
        }
    }

    public static void main(String[] args0) {
        for (int m = 0; m <= 5; m++) {
            for (int n = 0; n <= 5; n++) {
                System.out.printf("ack(%d, %d) = %d\n", m, n, ack(m, n));
            }
        }
    }
}
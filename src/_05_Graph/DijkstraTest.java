package _05_Graph;

import _05_Graph.Graph.DijkstraEdge;

public class DijkstraTest {
    public static void main(String[] args) {
//        graph from book p.121
        Graph graph = new Graph();
        Node a = new Node(new City("A", "A"));
        Node b = new Node(new City("B", "B"));
        Node c = new Node(new City("C", "C"));
        Node d = new Node(new City("D", "D"));
        Node e = new Node(new City("E", "E"));
        Node f = new Node(new City("F", "F"));
        Node g = new Node(new City("G", "G"));
        Node h = new Node(new City("H", "H"));

        graph.addNode(a);
        graph.addNode(b);
        graph.addNode(c);
        graph.addNode(d);
        graph.addNode(e);
        graph.addNode(f);
        graph.addNode(g);
        graph.addNode(h);

        graph.addEdge(a, b, 60);
        graph.addEdge(a, c, 20);
        graph.addEdge(b, c, 35);
        graph.addEdge(b, f, 110);
        graph.addEdge(c, d, 10);
        graph.addEdge(c, g, 75);
        graph.addEdge(d, b, 20);
        graph.addEdge(d, e, 95);
        graph.addEdge(e, c, 40);
        graph.addEdge(f, h, 30);
        graph.addEdge(g, f, 20);
        graph.addEdge(g, h, 55);

        graph.printAdjacency1();

        DijkstraEdge[] dEdges = graph.dijkstra(a);
        for (DijkstraEdge dEdge : dEdges) {
            System.out.printf("%s: %d %s\n", dEdge.node, dEdge.distance, dEdge.previous);
        }
        graph.findShortestWay(a, h);
    }
}

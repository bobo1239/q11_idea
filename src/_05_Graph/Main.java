package _05_Graph;

import _05_Graph.Graph.DijkstraEdge;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph();

        Node kc = new Node(new City("Kronach", "KC"));
        Node ny = new Node(new City("New York City", "NY"));
        Node tk = new Node(new City("Tokyo", "TK"));
        Node ln = new Node(new City("London", "LN"));
        Node pa = new Node(new City("Paris", "PA"));
//        Node sg = new Node(new City("Singapore", "SG"));
//        Node hk = new Node(new City("Hong Kong", "HK"));
//        Node du = new Node(new City("Dubai", "DU"));
//        Node bj = new Node(new City("Beijing", "BJ"));
//        Node sy = new Node(new City("Sydney", "SY"));
//        Node la = new Node(new City("Los Angeles", "LA"));

        graph.addNode(kc);
        graph.addNode(ny);
        graph.addNode(tk);
        graph.addNode(ln);
        graph.addNode(pa);
//        graph.addNode(sg);
//        graph.addNode(hk);
//        graph.addNode(du);
//        graph.addNode(bj);
//        graph.addNode(sy);
//        graph.addNode(la);


        graph.addBiEdge(kc, ny, 6363);
        graph.addBiEdge(kc, tk, 9197);
        graph.addBiEdge(kc, ln, 813);
        graph.addBiEdge(kc, pa, 666);
        graph.addBiEdge(ny, tk, 10842);
        graph.addBiEdge(ny, ln, 5567);
        graph.addBiEdge(ny, pa, 5834);
        graph.addBiEdge(tk, ln, 9553);
        graph.addBiEdge(tk, pa, 9706);
        graph.addBiEdge(ln, pa, 344);

        graph.printAdjacency1();
        graph.printAdjacency2();

        graph.traverseDepth(kc, (node) -> System.out.println(node.toString()));

        DijkstraEdge[] dEdges = graph.dijkstra(kc);
        for (DijkstraEdge dEdge : dEdges) {
            System.out.printf("%s: %d %s\n", dEdge.node, dEdge.distance, dEdge.previous);
        }
    }
}

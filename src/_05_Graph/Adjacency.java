package _05_Graph;

import java.util.List;

public interface Adjacency {
    void add(int n1, int n2, int weight);
    void remove(int n1, int n2);
    int get(int n1, int n2);
    void print1(List<Node> nodes);
    void print2(List<Node> nodes);
//    void shift(); TODO
}

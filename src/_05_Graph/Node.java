package _05_Graph;

public class Node {
    DataElement dataElement;

    public Node(DataElement dataElement) {
        this.dataElement = dataElement;
    }

    @Override
    public String toString() {
        return dataElement.toString();
    }
}

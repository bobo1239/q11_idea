package _05_Graph;

import java.util.List;

import static util.Utility.padStringTo;

public class AdjacencyMatrix implements Adjacency {
    int[][] matrix;
    int nodeCount;
    int length;

    public AdjacencyMatrix() {
        length = 0;
        nodeCount = 0;
    }

    //    0-based
    @Override
    public void add(int n1, int n2, int weight) {
        while (n1 >= length || n2 >= length) {
            grow();
        }
        matrix[n1][n2] = weight;
    }

    @Override
    public void remove(int n1, int n2) {
        if (n1 == n2) {
            matrix[n1][n2] = 0;
        } else {
            matrix[n1][n2] = -1;
        }
    }

    @Override
    public int get(int n1, int n2) {
        if (n1 >= length || n2 >= length) {
            if (n1 == n2) {
                return 0;
            } else {
                return -1;
            }
        }
        return matrix[n1][n2];
    }

    private void grow() {
        int factor = 3;
        if (length == 0) {
            extend(2);
        } else {
            extend(matrix.length * factor);
        }
    }

//    TODO: shrink()

    private void extend(int to) {
        if (matrix == null) {
            matrix = new int[to][to];
            for (int a = 0; a < to; a++) {
                for (int b = 0; b < to; b++) {
                    if (a == b) {
                        matrix[a][b] = 0;
                    } else {
                        matrix[a][b] = -1;
                    }
                }
            }
        } else {
            int[][] newMatrix = new int[to][to];
            for (int a = 0; a < to; a++) {
                for (int b = 0; b < to; b++) {
                    if (a < length && b < length) {
                        newMatrix[a][b] = matrix[a][b];
                    } else if (a == b) {
                        newMatrix[a][b] = 0;
                    } else {
                        newMatrix[a][b] = -1;
                    }
                }
            }
            matrix = newMatrix;
        }
        length = to;
    }

    public void print1(List<Node> nodes) {
        int width = 6;
        String empty = "";
        for (int i = 0; i < width; i++) {
            empty += " ";
        }

        System.out.print(empty);
        for (Node node : nodes) {
            System.out.print(padStringTo(node.dataElement.toString(), width));
        }
        System.out.print("\n");

        for (int i = 0; i < nodes.size(); i++) {
            System.out.print(padStringTo(nodes.get(i).dataElement.toString(), width));
            for (int j = 0; j < nodes.size(); j++)
                if (i < matrix.length && j < matrix.length && matrix[i][j] != -1) {
                    System.out.print(padStringTo(String.valueOf(matrix[i][j]), width));
                } else {
                    if (i == j) {
                        System.out.print(padStringTo("0", width));
                    } else
                        System.out.print(padStringTo("-1", width));
                }
            System.out.print("\n");
        }
    }

    @Override
    public void print2(List<Node> nodes) {
        for (int j = 0; j < nodes.size(); j++) {
            for (int i = 0; i < nodes.size(); i++) {
                System.out.print("matrix[" + j + "][" + i + "] = " + matrix[j][i] + "    \n");
            }
        }
    }
}

package _05_Graph;

public class City implements DataElement {
    private String name;
    private String abbreviation;

    public City(String name, String abbreviation) {
        this.name = name;
        this.abbreviation = abbreviation;
    }

    @Override
    public int compareTo(DataElement de) {
        City city = (City) de;
        return name.compareTo(city.getName());
    }

    @Override
    public boolean equals(Object de) {
        if (getClass() != de.getClass()) {
            return false;
        }
        City d = (City) de;
        return (compareTo(d) == 0);
    }

    public String toString() {
        return abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
}

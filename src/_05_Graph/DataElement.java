package _05_Graph;

public interface DataElement {
    int compareTo(DataElement de);

    @Override
    boolean equals(Object de);

    @Override
    String toString();
}

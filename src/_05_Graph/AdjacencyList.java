package _05_Graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AdjacencyList implements Adjacency {
    private class AdjacencyEntry {
        int n1, n2;
        int weight;

        AdjacencyEntry(int n1, int n2, int weight) {
            this.n1 = n1;
            this.n2 = n2;
            this.weight = weight;
        }
    }

    List<AdjacencyEntry> adjacencies = new ArrayList<>();

    @Override
    public void add(int n1, int n2, int weight) {
        for (AdjacencyEntry adj : adjacencies) {
            if (adj.n1 == n1 && adj.n2 == n2) {
//                TODO: throw error
                System.err.printf("Error: There is already an edge between %d und %d\n", n1, n2);
                return;
            }
        }
        adjacencies.add(new AdjacencyEntry(n1, n2, weight));
    }

    @Override
    public void remove(int n1, int n2) {
        adjacencies.stream()
                .filter(adj -> adj.n1 == n1 && adj.n2 == n2)
                .forEach(adjacencies::remove);
    }

    @Override
    public int get(int n1, int n2) {
        for (AdjacencyEntry adj : adjacencies) {
            if (adj.n1 == n1 && adj.n2 == n2) {
                return adj.weight;
            }
        }
        if (n1 == n2) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public void print1(List<Node> nodes) {
        List<AdjacencyEntry> tempList = new ArrayList<>(adjacencies);
        List<AdjacencyEntry> biDoNotRepeat = new ArrayList<>();
        for (AdjacencyEntry adj : adjacencies) {

            String n1 = nodes.get(adj.n1).toString();
            String n2 = nodes.get(adj.n2).toString();

            // check if it's an bidirectional edge
            boolean bidirectional = false;
            for (AdjacencyEntry adj2 : adjacencies) {
                if (adj.n1 == adj2.n2 && adj.n2 == adj2.n1 && adj.weight == adj2.weight) {
                    bidirectional = true;
                    biDoNotRepeat.add(adj2);
                    break;
                }
            }

            if (bidirectional) {
                if (!biDoNotRepeat.contains(adj)) {
                    System.out.printf("%s <-> %s : %d\n", n1, n2, adj.weight);
                }
            } else {
                System.out.printf("%s --> %s : %d\n", n1, n2, adj.weight);
            }
        }
    }

    @Override
    public void print2(List<Node> nodes) {
        print1(nodes);
    }
}

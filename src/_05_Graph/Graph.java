package _05_Graph;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.function.Consumer;

public class Graph {
    List<Node> nodes;
    Adjacency adjacency;

    public Graph() {
        nodes = new ArrayList<>();
        adjacency = new AdjacencyMatrix();
    }

    public void addNode(Node node) {
        nodes.add(node);
    }

    public void addEdge(Node n1, Node n2, int weight) {
        if (!nodes.contains(n1) || !nodes.contains(n2)) {
            System.err.println("Can't add edges to nodes which aren't in the graph"); // TODO: throw error
        }
        int n_n1 = nodes.indexOf(n1);
        int n_n2 = nodes.indexOf(n2);
        adjacency.add(n_n1, n_n2, weight);
    }

    public void addBiEdge(Node n1, Node n2, int weight) {
        addEdge(n1, n2, weight);
        addEdge(n2, n1, weight);
    }

    public void traverseDepth(Node startNode, Consumer<Node> func) {
        traverseDepth(startNode, func, new ArrayList<>());
    }

    private void traverseDepth(Node node, Consumer<Node> func, ArrayList<Node> visitedNodes) {
        visitedNodes.add(node);
        func.accept(node);

        int n = nodes.indexOf(node);
        for (int i = 0; i < nodes.size(); i++) {
            boolean isNeighbour = adjacency.get(n, i) >= 0;
            if (isNeighbour && !visitedNodes.contains(nodes.get(i))) {
                traverseDepth(nodes.get(i), func, visitedNodes);
            }
        }
    }

    public DijkstraEdge[] dijkstra(Node startNode) {
        DijkstraEdge[] dEdges = new DijkstraEdge[nodes.size()];
        for (int i = 0; i < dEdges.length; i++) {
            dEdges[i] = new DijkstraEdge(nodes.get(i));
            if (nodes.get(i) != startNode) {
                dEdges[i].distance = -1;
            }
        }
        PriorityQueue<DijkstraEdge> queue = new PriorityQueue<>();
        Collections.addAll(queue, dEdges);
        while (queue.size() > 0) {
            DijkstraEdge e = queue.poll();
            int nNode = nodes.indexOf(e.node);
            for (int i = 0; i < nodes.size(); i++) {
                int dist = adjacency.get(nNode, i);
                if (dist > 0) {
                    int newDist = e.distance + dist;
                    if (newDist < dEdges[i].distance || dEdges[i].distance == -1) {
                        dEdges[i].distance = newDist;
                        dEdges[i].previous = e.node;
                    }
                }
            }
        }
        return dEdges;
    }

    public void findShortestWay(Node startNode, Node endNode) {
        DijkstraEdge[] dEdges = dijkstra(startNode);
        Node searching = endNode;
        List<Node> path = new ArrayList<>();
        int distance = -1;
        while (true) {
            for (DijkstraEdge dEdge : dEdges) {
                if (dEdge.node == searching) {
                    if (dEdge.node == endNode) {
                        distance = dEdge.distance;
                    }
                    path.add(0, dEdge.node);
                    searching = dEdge.previous;
                    break;
                } else if (searching == null) {
                    System.out.println(path);
                    System.out.println(distance);
                    return;
                }
            }
        }
    }

    public void printAdjacency1() {
        adjacency.print1(nodes);
    }

    public void printAdjacency2() {
        adjacency.print2(nodes);
    }

    public class DijkstraEdge implements Comparable {
        public Node node;
        public int distance;
        public Node previous;

        private DijkstraEdge(Node node) {
            this.node = node;
        }

        @Override
        public int compareTo(Object o) {
            DijkstraEdge e2 = (DijkstraEdge) o;
            if (distance == -1) {
                return +1;
            } else if (e2.distance == -1) {
                return -1;
            } else {
                return distance - e2.distance;
            }
        }
    }
}

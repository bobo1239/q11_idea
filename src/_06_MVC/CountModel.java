package _06_MVC;

import java.util.Observable;

public class CountModel extends Observable {
    private int count;

    public CountModel() {
        count = 0;
    }

    public void countUp() {
        count++;
        setChanged();
        notifyObservers(count);
    }
}

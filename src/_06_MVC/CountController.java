package _06_MVC;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class CountController implements Observer, ActionListener {
    private CountModel model;
    private CountView view;

    public CountController(CountModel model, CountView view) {
        this.model = model;
        this.view = view;

        model.addObserver(this);
        view.getButton().addActionListener(this);
    }

    @Override public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("countUp")) {
            model.countUp();
        }
    }

    @Override public void update(Observable o, Object arg) {
        view.setLabelText(String.valueOf(arg));
    }
}

package _06_MVC;

import java.awt.*;
import javax.swing.*;

public class Main extends JFrame {
    private CountModel model;
    private CountView view;
    private CountController controller;

    public Main() throws HeadlessException {
        model = new CountModel();
        view = new CountView();
        controller = new CountController(model, view);

        this.setTitle("Counter");
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

        this.add(view);

        this.pack();
        setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        Main main = new Main();
    }
}

package _06_MVC;

import java.awt.*;
import javax.swing.*;

public class CountView extends JPanel {
    private JLabel label;
    private JButton button;

    public CountView() {
        this.setLayout(new FlowLayout(FlowLayout.CENTER));

        label = new JLabel("0");
        label.setPreferredSize(new Dimension(30, 20));
        this.add(label);

        button = new JButton("Press me!");
        button.setActionCommand("countUp");
        this.add(button);
    }

    public JButton getButton() {
        return button;
    }

    public void setLabelText(String text) {
        label.setText(text);
    }
}

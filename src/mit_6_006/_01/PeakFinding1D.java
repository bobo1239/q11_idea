package mit_6_006._01;

/*
* Definition peak:
* a[x-1] <= a[x] >= a[x+1]
* */

import java.util.Arrays;
import java.util.LinkedList;

class PeakFinding1D {
    static int findPeakNaive(int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (i == 0) {
                if (a[i] >= a[i+1]) {
                    return i;
                }
            } else if (i == a.length - 1) {
                if (a[i] >= a[i-1]) {
                    return i;
                }
            } else {
                if (a[i] >= a[i-1] && a[i] >= a[i+1]) {
                    return i;
                }
            }
        }
        return -1;
    }

    static int findPeakDnC(int[] a) {
        return findPeakDnC(a, 0, a.length - 1);
    }

    static int findPeakDnC(int[] a, int n, int m) {
        int mid = (n + m) / 2;
        if (mid != 0 && a[mid] < a[mid-1]) {
            return findPeakDnC(a, n, mid - 1);
        } else if (mid != a.length - 1 && a[mid] < a[mid+1]) {
            return findPeakDnC(a, mid + 1, m);
        } else {
            return mid;
        }
    }

    public static void main(String[] args) {
        LinkedList<int[]> arrays = new LinkedList<>();
        arrays.add(new int[] {1,2,3,4,5,4,3,2,1});
        arrays.add(new int[] {9,8,7,6,5,4,3,2,1});
        arrays.add(new int[] {1,2,3,4,5,6,7,8,9});
        arrays.add(new int[] {1,2,3,4,5,6,7,8,7});
        arrays.add(new int[] {7,8,7,6,5,4,3,2,1});
        for (int[] a : arrays) {
            System.out.println(Arrays.toString(a));
            System.out.println(findPeakNaive(a));
            System.out.println(findPeakDnC(a));
        }
    }
}

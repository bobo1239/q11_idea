package _01_linked_queue_generic;

class Main {
    public static void main(String[] args) {
        LinkedQueue<Patient> tq = new LinkedQueue<>();
        for (int i = 0; i <= 6; i++) {
            Patient patient = new Patient("p" + i);
            tq.add(patient);
        }

        System.out.println("FILLED!");
        System.out.println(tq);

        while (!tq.isEmpty()) {
            tq.poll();
        }

        System.out.println("EMPTIED!");
        System.out.println(tq);

        for (int i = 6; i <= 15; i++) {
            Patient patient = new Patient("p" + i);
            tq.add(patient);
        }

        System.out.println("FILLED!");
        System.out.println(tq);

        while (!tq.isEmpty()) {
            tq.poll();
        }

        System.out.println("EMPTIED!");
        System.out.println(tq);


        for (int i = 1; i <= 5; i++) {
            Patient patient = new Patient("p" + i);
            tq.add(patient);
        }

        Patient p = new Patient("Kill Me");
        tq.add(p);

        for (int i = 6; i <= 10; i++) {
            Patient patient = new Patient("p" + i);
            tq.add(patient);
        }
        System.out.println(tq);

        System.out.println(tq.remove(p));
        System.out.println("KILLED!");
        System.out.println(tq);

        System.out.println(tq.remove(new Patient("Not in List")));

    }
}

package _01_linked_queue_generic;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

class LinkedQueue<T> implements Queue<T> {
    private Node first, last;
    private int count = 0;

    @Override
    public void clear() {
        first = null;
        last = null;
    }

    @Override
    public boolean isEmpty() {
        return (first == null);
    }

    @Override
    public boolean remove(Object obj) {
        Node node1 = first;
        Node node2 = first.next;
        while (node2 != null) {
            if (node2.object == obj) {
                node1.next = node2.next;
                count--;
                return true;
            }
            node1 = node2;
            node2 = node2.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean add(T obj) throws NullPointerException {
        if (obj == null) {
            throw new NullPointerException();
        }

        if (last == null) {
//			empty queue
            first = new Node();
            first.object = obj;
            last = first;
        } else {
            last.next = new Node();
            last = last.next;
            last.object = obj;
        }
        count++;
        return true;
    }

    @Override
    public T element() throws NoSuchElementException {
        if (first == null) {
            throw new NoSuchElementException();
        }
        return first.object;
    }

    @Override
    public boolean offer(T obj) {
        try {
            this.add(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public T peek() {
        try {
            return element();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public T poll() {
        try {
            return this.remove();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public T remove() throws NoSuchElementException {
        if (first == null) {
            throw new NoSuchElementException();
        }

        T obj = first.object;
        first = first.next;
        count--;

        if (first == null) {
            last = null;
        }

        return obj;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("");
        Node node = first;
        while (node != null) {
            s.append(node.object.toString()).append("\n");
            node = node.next;
        }
        return s.toString().trim();
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean contains(Object obj) {
        Node node = first;

        while (node != null) {
            if (node.object == obj) {
                return true;
            }
            node = node.next;
        }

        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[count];
        Node node = first;
        int i = 0;
        while (node != null) {
            array[i] = node.object;
            node = node.next;
            i++;
        }
        return array;
    }

    @SuppressWarnings({"hiding", "TypeParameterHidesVisibleType"})
    @Override
    public <T> T[] toArray(T[] a) {
//        if (a.length >= count) {
////            for (int i == )
//            return null;
//        } else {
//            return (T[]) toArray();
//        }
//        TODO
        return null;
    }

    private class Node {
        T object;
        Node next;
    }
}

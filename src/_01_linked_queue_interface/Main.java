package _01_linked_queue_interface;

class Main {
    public static void main(String[] args) {
        LinkedQueue lq = new LinkedQueue();

        Car car = new Car();
        car.setWeight(15);
        lq.insert(car);

        Patient p = new Patient();
        p.weight = 50;
        lq.insert(p);

        System.out.println(lq.getTotalWeight());
        System.out.println(lq.getNodeCount());

        QueueElement qe;
        int weightSum = 0;
        while ((qe = lq.remove()) != null) {
            weightSum += qe.getWeight();
        }
        System.out.println(weightSum);
    }
}

package _01_linked_queue_interface;

class Car implements QueueElement {
    void setWeight(int weight) {
        this.weight = weight;
    }

    private int weight;

    @Override
    public int getWeight() {
        return weight;
    }
}

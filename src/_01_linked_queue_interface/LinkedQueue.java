package _01_linked_queue_interface;

class LinkedQueue {
    private Node first, last;

    void insert(QueueElement element) {
        if (first == null) {
//            empty Queue
            first = new Node(element);
            last = first;
        } else {
            last.next = new Node(element);
            last = last.next;
        }
    }

    QueueElement remove() {
        if (first == null) {
            return null;
        }

        QueueElement element = first.element;
        first = first.next;

        if (first == null) {
            last = null;
        }

        return element;
    }

    int getTotalWeight() {
        if (first == null) {
            return 0;
        }
        return first.getTotalWeight();
    }

    int getNodeCount() {
        if (first == null) {
            return 0;
        }
        return first.getNodeCount();
    }
}

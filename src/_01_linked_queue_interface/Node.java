package _01_linked_queue_interface;

class Node {
    Node next;
    QueueElement element;

    Node(QueueElement element) {
        this.element = element;
    }


    int getTotalWeight() {
        if (next == null) {
            return element.getWeight();
        } else {
            return element.getWeight() + next.getTotalWeight();
        }
    }

    int getNodeCount() {
        if (next == null) {
            return 1;
        }
        return next.getNodeCount() + 1;
    }
}

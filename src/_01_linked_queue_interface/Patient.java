package _01_linked_queue_interface;

class Patient implements QueueElement {
    int weight;

    @Override
    public int getWeight() {
        return weight;
    }
}

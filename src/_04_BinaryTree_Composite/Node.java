package _04_BinaryTree_Composite;

public class Node extends TreeElement {
    private DataElement data;
    private TreeElement left;
    private TreeElement right;

    public Node(DataElement data) {
        this.data = data;
        right = Terminator.getInstance();
        left = Terminator.getInstance();
    }

    public Node insert(DataElement newData) {
        int comp = newData.compareTo(data);
        if (comp == 0) {
            System.err.println("Tree already contains " + newData.toString());
        } else if (comp < 0) {
            left = left.insert(newData);
        } else {
            right = right.insert(newData);
        }
        return this;
    }

    public DataElement search(DataElement searchData) {
        if (data.equals(searchData)) {
            return data;
        }
        if (searchData.compareTo(data) < 0) {
            return left.search(searchData);
        } else {
            return right.search(searchData);
        }
    }

    @Override
    public String preOrderOut() {
        return String.format("%s %s %s", data.toString(), left.preOrderOut(), right.preOrderOut());
    }

    @Override
    public String inOrderOut() {
        return String.format("%s %s %s", left.inOrderOut(), data.toString(), right.inOrderOut());
    }

    @Override
    public String postOrderOut() {
        return String.format("%s %s %s", left.postOrderOut(), right.postOrderOut(), data.toString());
    }

    public TreeElement getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public TreeElement getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public DataElement getData() {
        return data;
    }

    public void setData(DataElement data) {
        this.data = data;
    }
}

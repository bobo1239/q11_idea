package _04_BinaryTree_Composite;

public interface DataElement {
    int compareTo(DataElement de);

    @Override
    boolean equals(Object de);

    @Override
    String toString();
}

package _04_BinaryTree_Composite;

public class DictionaryEntry implements DataElement {
    private String word;
    private String definition;

    public DictionaryEntry(String word, String definition) {
        this.word = word;
        this.definition = definition;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Override
    public int compareTo(DataElement de) {
        DictionaryEntry z = (DictionaryEntry) de;
        return word.compareTo(z.getWord());
    }

    @Override
    public boolean equals(Object de) {
        if (getClass() != de.getClass()) {
            return false;
        }
        DataElement d = (DictionaryEntry) de;
        return compareTo(d) == 0;
    }

    public String toString() {
        return word;
    }
}

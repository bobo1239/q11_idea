package _04_BinaryTree_Composite;

public class BinaryTree {
    TreeElement root;

    public BinaryTree() {
        root = Terminator.getInstance();
    }

    public void insert(DataElement de) {
        root = root.insert(de);
    }

    public boolean hasElement(DataElement de) {
        return root.search(de) != null;
    }

    public DataElement search(DataElement de) {
        return root.search(de);
    }

    public String preOrderOut() {
        return root.preOrderOut();
    }

    public String inOrderOut() {
        return root.inOrderOut();
    }

    public String postOrderOut() {
        return root.postOrderOut();
    }
}

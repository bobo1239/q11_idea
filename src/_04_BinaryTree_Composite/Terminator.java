package _04_BinaryTree_Composite;

public class Terminator extends TreeElement {
    private static Terminator instance = new Terminator();

    private Terminator() {}

    @Override
    public Node insert(DataElement de) {
        return new Node(de);
    }

    @Override
    public DataElement search(DataElement de) {
        return null;
    }

    @Override
    public String preOrderOut() {
        return "";
    }

    @Override
    public String inOrderOut() {
        return "";
    }

    @Override
    public String postOrderOut() {
        return "";
    }

    public static Terminator getInstance() {
        return instance;
    }
}

package _04_BinaryTree_Composite;

public abstract class TreeElement {
    public abstract Node insert(DataElement de);
    public abstract DataElement search(DataElement de);
    public abstract String preOrderOut();
    public abstract String inOrderOut();
    public abstract String postOrderOut();
}

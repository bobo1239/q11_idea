package _04_BinaryTree_Composite;

public class Main {
    public static void main(String[] args) {
        test();
//        BinaryTree bt = new BinaryTree();
//        DictionaryEntry d = new DictionaryEntry("Auto", "...");
//        bt.insert(d);
//        bt.insert(new DictionaryEntry("Aal", "..."));
//        bt.insert(new DictionaryEntry("Bass", "..."));
//        bt.insert(new DictionaryEntry("Baer", "..."));
//        System.out.println(bt.hasElement(new DictionaryEntry("Autos", "?")));
//        System.out.println(bt.search(d));
////        bt.insert(new Number(14));
////        bt.insert(new Number(15));
////        bt.insert(new Number(12));
////        bt.insert(new Number(13));
////        System.out.println(bt.search(new Number(14)));
////        System.out.println(bt.search(new Number(13)));
//        System.out.println(bt.preOrderOut());
//        System.out.println(bt.inOrderOut());
//        System.out.println(bt.postOrderOut());
    }

    private static void test() {
        BinaryTree bt = new BinaryTree();
        DictionaryEntry de1 = new DictionaryEntry("coca cola", "zuckerhaltiges Mistgetraenk");
        bt.insert(de1);
        bt.insert(new DictionaryEntry("crab", "Krabbe, Krebs, Griesgram"));
        bt.insert(new DictionaryEntry("coin", "Muenze, auspraegen, erfinden"));
        bt.insert(new DictionaryEntry("cube", "Wuerfel, dritte Potenz, in die dritte Potenz heben, in Wuerfel schneiden"));
        bt.insert(new DictionaryEntry("crow", "Kraehe, Freudenschrei, frohlocken, kraehen"));
        bt.insert(new DictionaryEntry("care", "Fürsorge"));
        bt.insert(new DictionaryEntry("car", "Auto"));
        System.out.println(bt.preOrderOut());
        System.out.println(bt.inOrderOut());
        System.out.println(bt.postOrderOut());
    }
}

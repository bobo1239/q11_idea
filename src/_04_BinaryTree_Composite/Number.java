package _04_BinaryTree_Composite;

public class Number implements DataElement {
    private int n;

    public Number(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public int compareTo(DataElement de) {
        Number z = (Number) de;
        return Integer.compare(this.n, z.getN());
    }

    @Override
    public boolean equals(Object de) {
        if (getClass() != de.getClass()) {
            return false;
        }
        DataElement d = (Number) de;
        return (compareTo(d) == 0);
    }

    public String toString() {
        return Integer.toString(n);
    }
}

package _03_linked_list_composite;

abstract class ListElement {
    abstract ListElement getNextQueueNode();

    abstract DataElement getDataElement();

    abstract void setNextListNode(ListElement node);

    abstract int getTotalWeight();

    abstract void insert(DataElement element, int pos);

    abstract DataElement getDataElement(int pos);

    abstract void listToString(StringBuilder sb);

    abstract int getNodeCount();

    abstract DataElement remove(int pos);

    abstract ListElement append(DataElement element);
}

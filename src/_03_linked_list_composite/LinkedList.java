package _03_linked_list_composite;

class LinkedList {
    ListElement first;

    LinkedList() {
        first = new Terminator();
    }

    void insert(DataElement element) {
        insert(element, 0);
    }

    void insert(DataElement element, int pos) {
        if (pos == 0) {
            ListElement next = first;
            first = new Node(element, next);
        } else {
            first.insert(element, pos);
        }
    }

    DataElement remove() {
        return remove(0);
    }

    DataElement remove(int pos) {
        if (pos == 0) {
            DataElement de = first.getDataElement();
            first = first.getNextQueueNode();
            return de;
        } else {
            return first.remove(pos);
        }
    }

    DataElement get() {
        return get(0);
    }

    DataElement get(int pos) {
        return first.getDataElement(pos);
    }

    int getTotalWeight() {
        return first.getTotalWeight();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        first.listToString(sb);
        return sb.toString();
    }

    int getNodeCount() {
        return first.getNodeCount();
    }

    public void append(DataElement element) {
        first = first.append(element);
    }
}

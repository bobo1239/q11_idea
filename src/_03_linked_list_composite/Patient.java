package _03_linked_list_composite;

class Patient implements DataElement {
    int weight = 80;

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return String.format("Patient (%d kg)", weight);
    }
}

package _03_linked_list_composite;

class Node extends ListElement {
    ListElement next;
    DataElement data;

    Node(DataElement element, ListElement next) {
        data = element;
        this.next = next;
    }

    @Override
    ListElement getNextQueueNode() {
        return next;
    }

    @Override
    DataElement getDataElement() {
        return data;
    }

    @Override
    void setNextListNode(ListElement node) {
        next = node;
    }

    @Override
    int getTotalWeight() {
        return next.getTotalWeight() + data.getWeight();
    }

    @Override
    void insert(DataElement element, int pos) {
        if (pos == 1) {
            ListElement node = new Node(element, next);
            next = node;
        } else {
            next.insert(element, pos - 1);
        }
    }

    @Override
    DataElement getDataElement(int pos) {
        if (pos == 0) {
            return data;
        } else {
            return next.getDataElement(pos - 1);
        }
    }

    @Override
    void listToString(StringBuilder sb) {
        sb.append("\n  - ");
        sb.append(data.toString());
        next.listToString(sb);
    }

    @Override
    int getNodeCount() {
        return next.getNodeCount() + 1;
    }

    @Override
    DataElement remove(int pos) {
        if (pos == 1) {
            DataElement de = next.getDataElement();
            next = next.getNextQueueNode();
            return de;
        } else {
            return next.remove(pos - 1);
        }
    }

    @Override
    ListElement append(DataElement element) {
        next = next.append(element);
        return this;
    }
}

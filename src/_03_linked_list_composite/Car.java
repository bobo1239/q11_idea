package _03_linked_list_composite;

class Car implements DataElement {
    public int weight;

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return String.format("Car (%d kg)", weight);
    }
}

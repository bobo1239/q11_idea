package _03_linked_list_composite;

class Terminator extends ListElement {
    @Override
    ListElement getNextQueueNode() {
        return this;
    }

    @Override
    DataElement getDataElement() {
        return null;
    }

    @Override
    void setNextListNode(ListElement node) {
    }

    @Override
    int getTotalWeight() {
        return 0;
    }

    @Override
    void insert(DataElement element, int pos) {
        System.err.println("List not long enough");
    }

    @Override
    DataElement getDataElement(int pos) {
        return null;
    }

    @Override
    void listToString(StringBuilder sb) {}

    @Override
    int getNodeCount() {
        return 0;
    }

    @Override
    DataElement remove(int pos) {
        return null;
    }

    @Override
    ListElement append(DataElement element) {
        ListElement next = new Node(element, this);
        return next;
    }
}

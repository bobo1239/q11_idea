package _03_linked_list_composite;

class Main {
    public static void main(String[] args) {
        LinkedList ll = new LinkedList();

        Car car = new Car();
        car.weight = 1500;
        ll.insert(car);

        Patient p = new Patient();
        p.weight = 50;
        ll.insert(p);

        car = new Car();
        car.weight = 5000;
        ll.append(car);

        ll.insert(new Patient(), 2);

        System.out.println(ll);

        System.out.println(ll.getTotalWeight());
        System.out.println(ll.getNodeCount());

        DataElement de;
        int weightSum = 0;
        while ((de = ll.remove()) != null) {
            weightSum += de.getWeight();
        }
        System.out.println(weightSum);

        System.out.println(ll);
    }
}

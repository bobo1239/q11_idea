package _03_linked_list_composite;

interface DataElement {
    int getWeight();
}

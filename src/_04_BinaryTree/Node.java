package _04_BinaryTree;

public class Node {
    private int value;
    private Node left;
    private Node right;

    public Node(int value) {
        this.value = value;
    }

    public void addValue(int newValue) {
        if (newValue < value) {
            if (left == null) {
                left = new Node(newValue);
            } else {
                left.addValue(newValue);
            }
        } else {
            if (right == null) {
                right = new Node(newValue);
            } else {
                right.addValue(newValue);
            }
        }
    }

    public boolean search(int searchValue) {
        if (value == searchValue) {
            return true;
        } else {
            Node node;

            if (searchValue < value) {
                node = left;
            } else {
                node = right;
            }

            return node != null && node.search(searchValue);
        }
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

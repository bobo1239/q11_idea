package _04_BinaryTree;

public class BinaryTree {
    Node root;

    public BinaryTree() {
        root = null;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public void addValue(int value) {
        if (root == null) {
            root = new Node(value);
        } else {
            root.addValue(value);
        }
    }

    public boolean search(int value) {
        return root != null && root.search(value);
    }
}

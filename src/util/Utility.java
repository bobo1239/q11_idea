package util;

public class Utility {
    public static String padStringTo(String s, int length) {
        if (s.length() == length) {
            return s;
        } else {
            StringBuilder pad = new StringBuilder();
            for (int i = 0; i < length - s.length(); i++) {
                pad.append(" ");
            }
            return s + pad.toString();
        }
    }
}
